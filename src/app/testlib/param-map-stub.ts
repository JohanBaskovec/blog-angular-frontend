export class ParamMapStub {
  values: { [key: string]: string | string[] } = {};

  /** Name of the parameters */
  get keys(): string[] {
    return Object.keys(this.values);
  }

  has(name: string): boolean {
    return this.values.hasOwnProperty(name);
  }

  /**
   * Return a single value for the given parameter name:
   * - the value when the parameter has a single value,
   * - the first value if the parameter has multiple values,
   * - `null` when there is no such parameter.
   */
  get(name: string): string | null {
    if (this.values[name] === undefined) {
      return null;
    } else if (typeof this.values[name] === 'string') {
      return this.values[name] as string;
    } else {
      return this.values[name][0];
    }
  }

  /**
   * Return an array of values for the given parameter name.
   *
   * If there is no such parameter, an empty array is returned.
   */
  getAll(name: string): string[] {
    if (typeof this.values[name] === 'string') {
      return [this.values[name]] as string[];
    } else {
      return this.values[name] as string[];
    }
  }
}
