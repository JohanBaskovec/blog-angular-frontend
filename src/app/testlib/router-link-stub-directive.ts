import {Directive, HostListener, Input} from '@angular/core';

/**
 * Copied from the official tutorial
 * https://angular.io/guide/testing#components-with-routerlink
 */
@Directive({
  selector: '[appRouterLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') routerLink: string;
  navigatedTo: any = null;

  @HostListener('click')
  onClick() {
    this.navigatedTo = this.routerLink;
  }
}
