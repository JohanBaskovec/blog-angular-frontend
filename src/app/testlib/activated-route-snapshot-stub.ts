import {ActivatedRouteSnapshot, Data, Params, Route, UrlSegment} from '@angular/router';
import {Type} from '@angular/core';
import {ParamMapStub} from './param-map-stub';

export class ActivatedRouteSnapshotStub {
  /** The URL segments matched by this route */
  url: UrlSegment[];
  /** The matrix parameters scoped to this route */
  params: Params;
  /** The query parameters shared by all the routes */
  queryParams: Params;
  /** The URL fragment shared by all the routes */
  fragment: string;
  /** The static and resolved data of this route */
  data: Data;
  /** The outlet name of the route */
  outlet: string;
  /** The component of the route */
  component: Type<any> | string | null;
  /** The configuration used to match this route **/
  routeConfig: Route | null;
  /** The root of the router state */
  root: ActivatedRouteSnapshot;
  /** The parent of this route in the router state tree */
  parent: ActivatedRouteSnapshot | null;
  /** The first child of this route in the router state tree */
  firstChild: ActivatedRouteSnapshot | null;
  /** The children of this route in the router state tree */
  children: ActivatedRouteSnapshot[];
  /** The path from the root of the router state tree to this route */
  pathFromRoot: ActivatedRouteSnapshot[];
  paramMap: ParamMapStub;
  queryParamMap: ParamMapStub;

  toString(): string {
    return '';
  }
}
