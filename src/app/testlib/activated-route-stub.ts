import {Observable} from 'rxjs';
import {Data, Params, Route, UrlSegment} from '@angular/router';
import {Type} from '@angular/core';
import {ParamMapStub} from './param-map-stub';
import {ActivatedRouteSnapshotStub} from './activated-route-snapshot-stub';

export class ActivatedRouteStub {
  /** An observable of the URL segments matched by this route */
  url: Observable<UrlSegment[]>;
  /** An observable of the matrix parameters scoped to this route */
  params: Observable<Params>;
  /** An observable of the query parameters shared by all the routes */
  queryParams: Observable<Params>;
  /** An observable of the URL fragment shared by all the routes */
  fragment: Observable<string>;
  /** An observable of the static and resolved data of this route. */
  data: Observable<Data>;
  /** The outlet name of the route. It's a constant */
  outlet: string;
  /** The component of the route. It's a constant */
  component: Type<any> | string | null;
  /** The current snapshot of this route */
  snapshot: ActivatedRouteSnapshotStub;
  /** The configuration used to match this route */
  routeConfig: Route | null;
  /** The root of the router state */
  root: ActivatedRouteStub;
  /** The parent of this route in the router state tree */
  parent: ActivatedRouteStub | null;
  /** The first child of this route in the router state tree */
  firstChild: ActivatedRouteStub | null;
  /** The children of this route in the router state tree */
  children: ActivatedRouteStub[];
  /** The path from the root of the router state tree to this route */
  pathFromRoot: ActivatedRouteStub[];
  paramMap: Observable<ParamMapStub>;
  queryParamMap: Observable<ParamMapStub>;

  toString(): string {
    return '';
  }
}
