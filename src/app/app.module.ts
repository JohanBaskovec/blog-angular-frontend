import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ArticlesListComponent} from './component/articles-list/articles-list.component';
import {ArticleFormComponent} from './component/article-form/article-form.component';
import {AppRoutingModule} from './app-routing.module';
import {ArticleComponent} from './component/article/article.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {TopMenuComponent} from './component/top-menu/top-menu.component';
import {ModalComponent} from './component/modal/modal.component';
import {LoginComponent} from './component/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticlesListComponent,
    ArticleFormComponent,
    ArticleComponent,
    TopMenuComponent,
    ModalComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
