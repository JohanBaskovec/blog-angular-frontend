import {Entity} from './entity';

export class Article extends Entity {
  constructor(
    public title: string = '',
    public content: string = '',
    public authorId: number = 0,
    id: number = 0
  ) {
    super(id);
  }
}
