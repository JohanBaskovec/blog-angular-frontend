import {Injectable} from '@angular/core';
import {Session} from '../../model/session';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  public currentSession: Session;

  constructor() {
  }
}
