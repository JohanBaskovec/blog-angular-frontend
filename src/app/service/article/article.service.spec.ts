import {inject, TestBed} from '@angular/core/testing';

import {ArticleService} from './article.service';
import {ApiService} from '../api/api.service';
import {Article} from '../../model/article';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;

describe('ArticleService', () => {
  let apiService: SpyObj<ApiService<Article>>;
  beforeEach(() => {
    apiService = jasmine.createSpyObj<ApiService<Article>>(
      [
        'getOne',
        'getAll',
        'post',
        'put'
      ]
    );
    TestBed.configureTestingModule({
      providers: [
        ArticleService,
        {
          provide: ApiService, useValue: (apiService)
        }
      ]
    });
  });

  it('should be created', inject([ArticleService], (service: ArticleService) => {
    expect(service).toBeTruthy();
  }));

  describe('getArticle', () => {
    it(
      'should return observable it gets from the apiService',
      inject(
        [ArticleService, ApiService],
        (service: ArticleService) => {
          const article = new Article();
          apiService.getOne.and.returnValue(of(article));
          const observable = service.getArticle(4);
          observable.subscribe((articleFromApiService) => expect(articleFromApiService).toBe(article));
        }
      )
    );
  });
});
