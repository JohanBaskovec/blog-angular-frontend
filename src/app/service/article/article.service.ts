import {Injectable} from '@angular/core';
import {Article} from '../../model/article';
import {Observable} from 'rxjs';
import {ApiService} from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  constructor(private apiService: ApiService<Article>) {
  }

  public getArticle(id: number): Observable<Article> {
    return this.apiService.getOne('article', id);
  }

  public getArticles(): Observable<Article[]> {
    return this.apiService.getAll('article');
  }

  public postArticle(article: Article): Observable<Article> {
    return this.apiService.post('article', article);
  }

  public putArticle(article: Article): Observable<never> {
    return this.apiService.put('article', article);
  }
}
