import {ApiOkResponse} from '../../model/api-ok-response';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MessagesService} from '../messages/messages.service';
import {Entity} from '../../model/entity';
import {SessionService} from '../session/session.service';

/**
 * Generic service for getting resources and handling errors.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService<T> {
  private apiUrl = 'http://localhost:8092';
  private readonly postHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true
  };

  constructor(
    private messagesService: MessagesService,
    private http: HttpClient,
    private sessionService: SessionService
  ) {
  }

  getOne(endpoint: 'article' | 'user', id: number): Observable<T> {
    return this.http
      .get<ApiOkResponse<T>>(`${this.apiUrl}/${endpoint}/${id}`)
      .pipe(
        map(response => response.data),
        catchError(err => {
          return this.handleError(err);
        })
      );
  }

  post(endpoint: 'article' | 'user' | 'session', data: any): Observable<T> {
    return this.http
      .post<ApiOkResponse<T>>(`${this.apiUrl}/${endpoint}`, data, this.postHttpOptions)
      .pipe(
        map(response => {
          return response.data;
        }),
        catchError(err => {
          return this.handleError(err);
        })
      );
  }

  getAll(endpoint: 'article' | 'user' | 'session'): Observable<T[]> {
    return this.http
      .get<ApiOkResponse<T[]>>(`${this.apiUrl}/${endpoint}`)
      .pipe(
        map(response => response.data),
        catchError(err => {
          return this.handleError(err);
        })
      );
  }

  public handleError(error: HttpErrorResponse, errorProcessors: { [status: number]: () => void } = {}): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred', error.error);
      this.messagesService.showAlert(`Error`);
    } else {
      console.error(`Backend returned error ${error.status}, body was ${error.error}`);
      if (errorProcessors[error.status] != null) {
        errorProcessors[error.status]();
      } else {
        switch (error.status) {
          case 0:
            this.messagesService.showAlert('Server offline.');
            break;
          case 401:
            this.messagesService.showAlert('You are not logged in.');
            // the frontend may have a session while it has expired in the backend,
            // so a 401 error will be returned when doing a request that requires
            // authorization, even though the frontend still had a session object,
            // in this case we delete the session object
            this.sessionService.currentSession = null;
            break;
          case 404:
            this.messagesService.showAlert('Not found.');
            break;
          default:
            this.messagesService.showAlert(`Server internal error.`);
            break;
        }
      }
    }
    return throwError(error);
  }

  put(endpoint: string, data: Entity): Observable<never> {
    return this.http
      .put<never>(`${this.apiUrl}/${endpoint}/${data.id}`, data, this.postHttpOptions)
      .pipe(
        catchError(err => {
          return this.handleError(err);
        })
      );
  }
}
