import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  public message: string;
  public onCancel: () => void;
  public onOk: () => void;

  constructor() {
  }

  private _hidden: boolean;

  get hidden(): boolean {
    return this._hidden;
  }

  public close() {
    this._hidden = true;
    this.message = '';
    this.onCancel = null;
    this.onOk = null;
  }

  public open() {
    this._hidden = false;
  }
}
