import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  public message = '';
  public onCancel: () => void;
  public onOk: () => void;

  constructor() {
  }

  private _hidden = true;

  get hidden(): boolean {
    return this._hidden;
  }

  public close() {
    this._hidden = true;
    this.message = '';
    this.onCancel = null;
    this.onOk = null;
  }

  public open() {
    this._hidden = false;
  }

  /**
   * Show a simple alert with an OK button
   * @param message The message
   */
  showAlert(message: string) {
    this.message = message;
    this.onOk = () => this.close();
    this.open();
  }
}
