import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleComponent} from './article.component';
import {ActivatedRoute} from '@angular/router';
import {ArticleService} from '../../service/article/article.service';
import {ActivatedRouteStub} from '../../testlib/activated-route-stub';
import {ActivatedRouteSnapshotStub} from '../../testlib/activated-route-snapshot-stub';
import {ParamMapStub} from '../../testlib/param-map-stub';
import {of} from 'rxjs';
import {Article} from '../../model/article';
import {RouterTestingModule} from '@angular/router/testing';
import SpyObj = jasmine.SpyObj;

describe('ArticleComponent', () => {
  let component: ArticleComponent;
  let fixture: ComponentFixture<ArticleComponent>;
  let activatedRouteStub: ActivatedRouteStub;
  let articleServiceStub: SpyObj<ArticleService>;


  beforeEach(async(() => {
    activatedRouteStub = new ActivatedRouteStub();
    articleServiceStub = jasmine.createSpyObj<ArticleService>(['getArticle']);
    TestBed.configureTestingModule({
      declarations: [ArticleComponent],
      imports: [
        RouterTestingModule,
      ],
      providers: [
        {provide: ArticleService, useValue: articleServiceStub},
        {provide: ActivatedRoute, useValue: activatedRouteStub}
      ]
    })
      .compileComponents();
  }));

  it('should create', () => {
    const activatedRoute: ActivatedRouteStub = TestBed.get(ActivatedRoute);
    const snapshot = new ActivatedRouteSnapshotStub();
    const paramMap = new ParamMapStub();
    paramMap.values['id'] = '3';
    snapshot.paramMap = paramMap;
    activatedRoute.snapshot = snapshot;

    const articleService: SpyObj<ArticleService> = TestBed.get(ArticleService);
    const article = new Article('title3', 'content3', 4, 3);
    articleService.getArticle.and.returnValue(of(article));

    fixture = TestBed.createComponent(ArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
