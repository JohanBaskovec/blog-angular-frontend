import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../model/article';
import {ArticleService} from '../../service/article/article.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  @Input()
  public article: Article;


  constructor(
    private articleService: ArticleService,
    private route: ActivatedRoute,
  ) {

  }

  ngOnInit() {
    if (this.article === undefined) {
      this.getArticle();
    }
  }

  getArticle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.articleService.getArticle(id)
      .subscribe(article => {
        console.log(article);
        this.article = article;
      });
  }
}
