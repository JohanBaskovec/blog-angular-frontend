import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../service/article/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Article} from '../../model/article';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {
  public article: Article;
  public isCreation = false;
  public formMessage = '';

  constructor(
    private articleService: ArticleService,
    private route: ActivatedRoute,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.isCreation = this.route.snapshot.data.creation;
    if (this.isCreation) {
      this.article = new Article();
    } else {
      const idString = this.route.snapshot.paramMap.get('id');
      if (idString != null) {
        this.getArticle(+idString);
      }
    }
  }

  getArticle(id: number): void {
    this.articleService.getArticle(id)
      .subscribe(article => {
        console.log(article);
        this.article = article;
      });
  }

  onSubmit(): void {
    if (this.isCreation) {
      this.articleService.postArticle(this.article)
        .subscribe((article) => {
          this.article = article;
          this.router.navigateByUrl(`/article/${article.id}`);
        });
    } else {
      this.articleService.putArticle(this.article).subscribe({
        complete: () => {
          this.formMessage = 'The article has been modified.';
        }
      });
    }
  }
}
