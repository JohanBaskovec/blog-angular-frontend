import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleFormComponent} from './article-form.component';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {ArticleService} from '../../service/article/article.service';
import {ActivatedRouteStub} from '../../testlib/activated-route-stub';
import {ActivatedRoute} from '@angular/router';
import {ActivatedRouteSnapshotStub} from '../../testlib/activated-route-snapshot-stub';
import SpyObj = jasmine.SpyObj;

describe('ArticleFormComponent', () => {
  let component: ArticleFormComponent;
  let fixture: ComponentFixture<ArticleFormComponent>;
  let activatedRouteStub: ActivatedRouteStub;
  let articleServiceStub: SpyObj<ArticleService>;

  beforeEach(async(() => {
    activatedRouteStub = new ActivatedRouteStub();
    articleServiceStub = jasmine.createSpyObj<ArticleService>(['getArticle']);
    TestBed.configureTestingModule({
      declarations: [ArticleFormComponent],
      imports: [
        FormsModule,
        RouterTestingModule,
      ],
      providers: [
        {provide: ArticleService, useValue: articleServiceStub},
        {provide: ActivatedRoute, useValue: activatedRouteStub}
      ]
    })
      .compileComponents();
  }));

  it('should create', () => {
    const activatedRoute: ActivatedRouteStub = TestBed.get(ActivatedRoute);
    const snapshot = new ActivatedRouteSnapshotStub();
    snapshot.data = {creation: true};
    activatedRoute.snapshot = snapshot;

    fixture = TestBed.createComponent(ArticleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
