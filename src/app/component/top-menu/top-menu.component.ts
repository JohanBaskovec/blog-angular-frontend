import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../service/session/session.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  constructor(public sessionService: SessionService) {
  }

  ngOnInit() {
  }

}
