import {Component, OnInit} from '@angular/core';
import {MessagesService} from '../../service/messages/messages.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  constructor(public messagesService: MessagesService) {
  }

  ngOnInit() {
  }
}
