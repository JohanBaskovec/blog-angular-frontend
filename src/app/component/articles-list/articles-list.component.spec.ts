import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticlesListComponent} from './articles-list.component';
import {Component, Input} from '@angular/core';
import {Article} from '../../model/article';
import {ArticleService} from '../../service/article/article.service';
import {SessionService} from '../../service/session/session.service';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;

@Component({
  selector: 'app-article',
  template: '',
})
class ArticleStubComponent {
  @Input()
  public article: Article;
}

describe('ArticlesListComponent', () => {
  let component: ArticlesListComponent;
  let fixture: ComponentFixture<ArticlesListComponent>;
  let articleServiceStub: SpyObj<ArticleService>;
  let sessionServiceStub: SpyObj<SessionService>;

  beforeEach(async(() => {
    articleServiceStub = jasmine.createSpyObj<ArticleService>(
      [
        'getArticles'
      ]);
    sessionServiceStub = {} as SpyObj<SessionService>;
    TestBed.configureTestingModule({
      declarations: [
        ArticlesListComponent,
        ArticleStubComponent
      ],
      providers: [
        {provide: ArticleService, useValue: articleServiceStub},
        {provide: SessionService, useValue: sessionServiceStub}
      ]
    })
      .compileComponents();
  }));

  it('should create', () => {
    const articleService: SpyObj<ArticleService> = TestBed.get(ArticleService);
    articleService.getArticles.and.returnValue(of([]));

    fixture = TestBed.createComponent(ArticlesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
