import {Component, OnInit} from '@angular/core';
import {Article} from '../../model/article';
import {ArticleService} from '../../service/article/article.service';
import {SessionService} from '../../service/session/session.service';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.scss']
})
export class ArticlesListComponent implements OnInit {
  public articles = [];

  constructor(
    private articleService: ArticleService,
    public sessionService: SessionService
  ) {
    console.log('constructed');
    console.log(articleService);
  }

  ngOnInit() {
    console.log('on init');
    this.articleService.getArticles()
      .subscribe(
        (articles: Article[]) => {
          this.articles = articles;
        },
      );
  }
}
