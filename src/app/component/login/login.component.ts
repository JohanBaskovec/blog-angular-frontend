import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../service/api/api.service';
import {SessionService} from '../../service/session/session.service';
import {User} from '../../model/user';
import {Session} from '../../model/session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public formMessage = '';
  public username = '';
  public password = '';

  constructor(
    public apiService: ApiService<User>,
    public sessionService: SessionService
  ) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.apiService.post('session', {
      username: this.username,
      password: this.password
    })
      .subscribe((response) => {
        const newSession = new Session();
        newSession.user = response;
        this.sessionService.currentSession = newSession;
      });
  }
}
