import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ArticleFormComponent} from './component/article-form/article-form.component';
import {ArticlesListComponent} from './component/articles-list/articles-list.component';
import {ArticleComponent} from './component/article/article.component';
import {LoginComponent} from './component/login/login.component';

const routes = [
  {path: '', component: ArticlesListComponent},
  {path: 'article/:id/edit', component: ArticleFormComponent, data: {creation: false}},
  {path: 'article/new', component: ArticleFormComponent, data: {creation: true}},
  {path: 'article/:id', component: ArticleComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled'
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
